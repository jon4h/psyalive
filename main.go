package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/urfave/cli/v2"
)

var logErr = log.New(os.Stderr, "", 0)

func main() {

	var port string
	var notification string
	var timeout int

	app := &cli.App{
		EnableBashCompletion: true,
		Version:              "v1.1",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "server",
				Aliases:     []string{"s"},
				Destination: &server,
				Usage:       "Server to check",
				Required:    true,
			},
			&cli.StringFlag{
				Name:        "port",
				Aliases:     []string{"p"},
				Destination: &port,
				Usage:       "TCP Port to test",
				Required:    true,
			},
			&cli.StringFlag{
				Name:        "notification",
				Aliases:     []string{"n"},
				Value:       "stderr",
				Destination: &notification,
				Usage:       "Service to send notifications. Currently is only stderr and telegram available. If not set it will notify over stderr. For telegram please use the syntax: tg://BOTTOKEN/ID",
				Required:    false,
			},
			&cli.IntFlag{
				Name:        "timeout",
				Aliases:     []string{"t"},
				Value:       180,
				Destination: &timeout,
				Usage:       "Timeout in Seconds. Default is 3min = 180",
				Required:    false,
			},
		},

		Action: func(c *cli.Context) error {
			alive := aliveTest(server, port, timeout)
			if !alive {
				sendNotification(notification, server, port)
			}
			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func aliveTest(server string, port string, timeout int) bool {
	ip, err := net.LookupIP(server)
	if err != nil {
		logErr.Println(err)
	} else {
		address := fmt.Sprintf("%s:%s", ip[0], port)
		timeout := net.Dialer{Timeout: time.Duration(timeout) * time.Second}
		conn, err := timeout.Dial("tcp", address)
		if err != nil {
			return false
		} else {
			conn.Close()
			return true
		}

	}
	return false
}

func sendNotification(notification string, server string, port string) {
	if notification == "stderr" {
		logErr.Printf("Warning:\nThe Server %s is unavailable!\nTested Port: %s\n", server, port)
	} else {
		splits := strings.Split(notification, "://")
		if splits[0] == "tg" {
			telegram := strings.Split(splits[1], "/")
			URI := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage?chat_id=%s&parse_mode=HTML&text=<b>%%E2%%9A%%A0Warning%%0a</b>The Server %s is unavailable!%%0aTested Port: %s", telegram[0], telegram[1], server, port)
			http.Get(URI)
		} else {
			fmt.Println("Unsupported Notification Parameter! Using stderr.")
			sendNotification("stderr", server, port)
		}
	}

}

/* Notes:
emojis in URI: https://github.com/gasparandr/emoji-express/blob/master/data/emojis.js
*/
