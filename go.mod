module psyalive

go 1.13

require (
	github.com/akamensky/argparse v1.1.0
	github.com/urfave/cli/v2 v2.1.1
)
