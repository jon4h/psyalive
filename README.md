# Psyalive
A simple Tool to check if a server is alive.

## Install

### Software dependencies
* [Git](https://git-scm.com/downloads)
* [Golang](https://golang.org/dl/)

### Prerequisites
* Working Go environnement. [Example](https://www.tutorialspoint.com/go/go_environment.htm)

### Build
* Start a console
* Clone the repository: `git clone https://gitlab.com/jon4h/psyalive`
* Run `cd psyalive`
* Build the package: `go build .`

Now you can execute the the programm using `./psyalive`. If you copy/move the "psyalive" binary into a folder which is in your environnement PATH you can simply execute `psyalive`


### Examples
With the following command you can test a server by a given port. If the server responds over the port, nothing happens. Otherwise it will wirte an error to the systems stderr.

`psyalive -s server -p port`

ex: `psyalive -s google.com -p 443`

#### Notification over Telegram
If you want a notification on Telegram if your server is down, use the following syntax:

`psyalive -s server -p port -n tg://BOTTOKEN/ID`

You will need a Telegram Bot to run this. More information [here](https://core.telegram.org/bots)

#### Set a custon TCP Timeout

The default timeout is 180 seconds. If you want to change this, use the parameter -t seconds.